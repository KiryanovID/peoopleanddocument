package ru.kiryanov.application.errors;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class BadRequest extends RuntimeException{
  private final String MESSAGE = "Empty field";
  private final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;



}
