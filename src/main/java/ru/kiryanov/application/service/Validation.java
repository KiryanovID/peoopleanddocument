package ru.kiryanov.application.service;

import org.springframework.stereotype.Component;
import ru.kiryanov.application.dto.PersonRequest;
import ru.kiryanov.application.errors.BadRequest;

@Component
public class Validation {

  public static void PersonValid(PersonRequest person){
    if((person.getFirstName().trim().equals("") || person.getFirstName() == null) ||
        (person.getLastName().trim().equals("") || person.getLastName() == null) ||
        (person.getPatronymic().trim().equals("") || person.getPatronymic() == null) ||
        (person.getBirthday() == null) ||
        (person.getSex() == null)){
      throw new BadRequest();
    }

  }
}
