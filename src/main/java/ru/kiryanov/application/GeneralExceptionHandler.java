package ru.kiryanov.application;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.kiryanov.application.dto.ErrorDto;
import ru.kiryanov.application.errors.BadRequest;

@ControllerAdvice
@Slf4j
public class GeneralExceptionHandler {

  @ExceptionHandler(BadRequest.class)
  public static ResponseEntity<Object> nameError(BadRequest badRequest){
    log.error("Entry not added");
    var x = badRequest.getHttpStatus().value();
    var z = badRequest.getMESSAGE();
    //new ErrorDto(badRequest.getHttpStatus().value(), badRequest.getMESSAGE()), HttpStatus.BAD_REQUEST
    var r = new ErrorDto(x,z);

    return new ResponseEntity<>(r, HttpStatus.BAD_REQUEST);
  }
}
