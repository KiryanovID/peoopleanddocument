package ru.kiryanov.application.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kiryanov.application.service.Validation;
import ru.kiryanov.application.dto.GodDto;
import ru.kiryanov.application.dto.PersonRequest;
import ru.kiryanov.application.dto.PersonResponse;
import ru.kiryanov.application.errors.BadRequest;
import ru.kiryanov.infrastructure.repositories.PersonRepositories;

@RestController
@CrossOrigin
@RequestMapping("api/person")
@Slf4j
public class PersonController {
  private final PersonRepositories repositories;

  public PersonController(PersonRepositories repositories) {
    this.repositories = repositories;
  }

  @GetMapping
  public ResponseEntity<Iterable<PersonResponse>> getAll(){
    log.info("test");
    var x=  repositories.findAll().orElseThrow(BadRequest::new);
    return ResponseEntity.ok(x.stream().map(PersonResponse::new).toList());
  }

  @PostMapping()
  public ResponseEntity<GodDto> savePerson(@RequestBody PersonRequest person) {
    Validation.PersonValid(person);
    repositories.save(person);
    log.info("Пользователь добавлен {}", person.toString());
    return new ResponseEntity<>(new GodDto(HttpStatus.OK.value(), "Person added"), HttpStatus.OK);
  }
}
