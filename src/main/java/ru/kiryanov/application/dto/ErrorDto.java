package ru.kiryanov.application.dto;

import lombok.Data;

@Data
public class ErrorDto {
  private int httpStatus;
  private String message;

  public ErrorDto(int httpStatus, String message) {
    this.httpStatus = httpStatus;
    this.message = message;
  }
}
