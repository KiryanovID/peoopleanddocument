package ru.kiryanov.application.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.kiryanov.domain.entity.Sex;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class PersonRequest {
  @Getter private String firstName;
  @Getter private String lastName;
  @Getter private String patronymic;
  @Getter private LocalDate birthday;
  @Getter private Sex sex;
  @Getter private List<DocumentRequest> documents;

  @Override
  public String toString() {
    return  lastName + " " + firstName + " " + patronymic;
  }
}
