package ru.kiryanov.application.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.kiryanov.domain.entity.TypeDocuments;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
public class DocumentRequest {
  @JsonProperty("typeId")
  @Getter private TypeDocuments type;
  @Getter private int peopleId;
  @Getter private int series;
  @Getter private int number;
  @Getter private LocalDate dateOfIssue;
}
