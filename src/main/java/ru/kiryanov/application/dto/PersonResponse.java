package ru.kiryanov.application.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.kiryanov.infrastructure.models.PersonModel;

import java.time.LocalDate;
import java.time.Period;

@NoArgsConstructor
public class PersonResponse {
  @Getter private String firstName;
  @Getter private String lastName;
  @Getter private String patronymic;
  @Getter private int age;

  @Getter private int countDocument;


  public PersonResponse(PersonModel person) {
    this.firstName = person.getFirstName();
    this.lastName = person.getLastName();
    this.patronymic = person.getPatronymic();
    this.age = Period.between(person.getBirthday(), LocalDate.now()).getYears();
    this.countDocument = person.getDocuments().size();
  }
}
