package ru.kiryanov.application.dto;

import lombok.Data;

@Data
public class GodDto {
  private int httpStatus;
  private String message;

  public GodDto(int httpStatus, String message) {
    this.httpStatus = httpStatus;
    this.message = message;
  }
}
