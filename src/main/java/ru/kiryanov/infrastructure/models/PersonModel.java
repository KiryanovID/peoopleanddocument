package ru.kiryanov.infrastructure.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.kiryanov.application.dto.PersonRequest;
import ru.kiryanov.domain.entity.Sex;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "people")
public class PersonModel {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true,nullable = false)
  @Getter
  private int id;

  @Getter
  private String firstName;

  @Getter
  private String lastName;

  @Getter
  private String patronymic;

  @Getter
  private LocalDate birthday;

  @Enumerated(EnumType.STRING)
  @Getter
  private Sex sex;

  @Getter
  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name = "people_id")
  private List<DocumentModel> documents;

  protected PersonModel() {
  }
  public PersonModel(PersonRequest person){
    this.firstName = person.getFirstName();
    this.lastName = person.getLastName();
    this.patronymic = person.getPatronymic();
    this.birthday = person.getBirthday();
    this.documents =  person.getDocuments().stream().map(DocumentModel::new).toList();
    this.sex = person.getSex();
  }
}
