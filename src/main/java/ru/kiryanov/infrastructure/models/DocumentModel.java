package ru.kiryanov.infrastructure.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import ru.kiryanov.application.dto.DocumentRequest;
import ru.kiryanov.domain.entity.TypeDocuments;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "document")
public class DocumentModel {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(unique = true, nullable = false)
  @Getter
  private int id;

  @Getter
  @Enumerated(EnumType.STRING)
  @JsonProperty("typeId")
  private TypeDocuments type;

  @Getter
  private int series;

  @Getter
  private int number;

  @Getter
  private LocalDate dateOfIssue;

  @Getter
  @ManyToOne
  private PersonModel people;
  protected DocumentModel() {
  }

  public DocumentModel(DocumentRequest document) {
    this.type = document.getType();
    this.series = document.getSeries();
    this.number = document.getNumber();
    this.dateOfIssue= document.getDateOfIssue();
  }
}
