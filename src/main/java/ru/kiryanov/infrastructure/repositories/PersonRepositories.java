package ru.kiryanov.infrastructure.repositories;

import org.springframework.stereotype.Repository;
import ru.kiryanov.application.dto.PersonRequest;
import ru.kiryanov.infrastructure.models.PersonModel;

import java.util.List;
import java.util.Optional;

@Repository
public class PersonRepositories {

  private final Persons persons;

  public PersonRepositories(Persons persons) {
    this.persons = persons;
  }

  public void save(PersonRequest person){
    persons.save(new PersonModel(person));
  }
  public Optional<List<PersonModel>> findAll(){
    return Optional.of(persons.findAll());
  }
}
