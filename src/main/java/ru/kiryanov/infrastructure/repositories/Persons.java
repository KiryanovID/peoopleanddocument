package ru.kiryanov.infrastructure.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.kiryanov.infrastructure.models.PersonModel;

import java.util.List;

public interface Persons extends JpaRepository<PersonModel, Long> {
}