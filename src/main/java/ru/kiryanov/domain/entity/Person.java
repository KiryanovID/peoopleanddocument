package ru.kiryanov.domain.entity;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


/**
 * Сущность человека.
 */
@NoArgsConstructor
@AllArgsConstructor
public class Person {
  /**
   * Идентификатор.
   */
  @Getter private Long id;
  /**
   * Имя.
   */
  @Getter private String firstName;
  /**
   * Фамилия.
   */
  @Getter private String lastName;
  /**
   * Отчество.
   */
  @Getter private String patronymic;
  /**
   * Дата рождения.
   */
  @Getter private LocalDate birthday;
  /**
   * Пол.
   */
  @Getter private Sex sex;
}
