package ru.kiryanov.domain.entity;

import lombok.Getter;

public enum TypeDocuments {
  PASSPORT(1, "Паспорт"),
  SNILS(2, "СНИЛС"),
  INN(3, "ИНН"),
  DRIVER_CARD(4, "Водительское удостоверение"),
  MILITARY_ID(5, "Военный билет"),
  CERTIFICATE_OF_ATTRIBUTION(6, "Приписное свидетельство");

  @Getter private final int id;
  @Getter private final String name;

  TypeDocuments(int id, String name) {
    this.id = id;
    this.name = name;
  }
}
