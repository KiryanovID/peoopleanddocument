package ru.kiryanov.domain.entity;

import lombok.Getter;

public enum Sex {
  MALE('м', "Мужской"),
  FEMALE('ж', "Женский");

  @Getter
  private final char ch;
  @Getter
  private final String sex;
  Sex(char ch, String sex) {
    this.ch = ch;
    this.sex = sex;
  }
}
