package ru.kiryanov.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * Документ.
 */
@NoArgsConstructor
@AllArgsConstructor
public class Document {
  /**
   * Идентификатор.
   */
  @Getter private Long id;
  /**
   * Идентификатор типа документа.
   */
  @Getter private Long typeId;
  /**
   * идентификатор человека.
   */
  @Getter private Long peopleId;
  /**
   * Серия документа.
   */
  @Getter private int series;
  /**
   * Номер документа.
   */
  @Getter private int number;
  /**
   * Дата выдачи документа.
   */
  @Getter private LocalDate dateOfIssue;

}
