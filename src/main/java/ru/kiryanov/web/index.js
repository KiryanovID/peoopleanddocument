function maleAdd(){
    document.getElementById("formMI").hidden = false;
    document.getElementById("formPS").hidden = false;
}
function femaleAdd(){
    document.getElementById("formMI").hidden = true;
    document.getElementById("formPS").hidden = true;
}
async function addInfo(){
    let pass = createPassport();
    let snils = createSnils();
    let inn = createInn();
    let dc = createDriverCard();

    let firstName = document.getElementById("fName").value;
    let lastName = document.getElementById("lName").value;
    let patronymic = document.getElementById("patro").value;
    let birthday = document.getElementById("date").value;
    let sex = document.querySelector('input[name="sex"]:checked').value;
    let documents = [pass, snils, inn, dc];

    let user = new People(firstName, lastName, patronymic, birthday, sex, documents);

    fetch('http://localhost:8080/api/person', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(user)
    }).then(response =>{
        if(response.ok){
            alert("Человек добавлен")
        }
        else {
            alert("произошла ошибка")
        }
    });

    let elements = document.querySelectorAll("input");
    elements.forEach(element => {
        element.value = "";
    })
    document.querySelector('input[name="sex"]:checked').checked = false;
}


document.getElementById("listBtn").onclick = async function(){
    document.getElementById("table").hidden = false;
    let response = await fetch('http://localhost:8080/api/person');
    let content = await response.json();
    let list = document.getElementById("peoplesList")

    for(let key in content){
        let row = `<tr>
           <td>${content[key].lastName}</td>
            <td>${content[key].firstName}</td>
            <td>${content[key].patronymic}</td>
            <td>${content[key].age}</td>
            <td>${content[key].countDocument}</td>
            </tr>`
        list.innerHTML += row;
    }
}

class People{
    constructor(firstName, lastName, patronymic, birthday, sex, documents) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.birthday = birthday;
        this.sex =sex;
        this.documents = documents;
    }
}

class Passport{
    typeId = "PASSPORT";
    constructor(series, number, dateOfIssue){
        this.typeId;
        this.series = series;
        this.number = number;
        this.dateOfIssue = dateOfIssue;
    }
}
class Snils{
    typeId = "SNILS";
    constructor(series, number, dateOfIssue){
        this.typeId;
        this.series = series;
        this.number = number;
        this.dateOfIssue = dateOfIssue;
    }
}
class Inn{
    typeId = "INN";
    constructor(series, number, dateOfIssue){
        this.typeId;
        this.series = series;
        this.number = number;
        this.dateOfIssue = dateOfIssue;
    }
}
class DriverCard{
    typeId = "DRIVER_CARD";
    constructor(series, number, dateOfIssue){
        this.typeId;
        this.series = series;
        this.number = number;
        this.dateOfIssue = dateOfIssue;
    }
}class MILITARY_ID{
    typeId = "MILITARY_ID";
    constructor(series, number, dateOfIssue){
        this.typeId;
        this.series = series;
        this.number = number;
        this.dateOfIssue = dateOfIssue;
    }
}class CERTIFICATE_OF_ATTRIBUTION{
    typeId = "CERTIFICATE_OF_ATTRIBUTION";
    constructor(series, number, dateOfIssue){
        this.typeId;
        this.series = series;
        this.number = number;
        this.dateOfIssue = dateOfIssue;
    }
}

function createPassport(){
    return new Passport(document.getElementById("passSer").value,
        document.getElementById("passNum").value,
        document.getElementById("passDate").value
    );
}
function createSnils(){
    return new Snils(document.getElementById("snilsSer").value,
        document.getElementById("snilsNum").value,
        document.getElementById("passDate").value
    );
}
function createInn(){
    return new Inn(document.getElementById("innSer").value,
        document.getElementById("innNum").value,
        document.getElementById("innDate").value
    );
}
function createDriverCard(){
    return new DriverCard(document.getElementById("dcSer").value,
        document.getElementById("dcNum").value,
        document.getElementById("dcDate").value
    );
}
